import java.io.StringWriter;

import com.app.test.jnut.shape.exception.ShapeCreationException;
import com.app.test.jnut.shape.shape.FactoryShape;

public class Shapes {

	private ShapeWriter writer;

	public Shapes(StringWriter out) {
		writer = new ShapeWriter(out);
	}

	public void area(String nameOfShape, String dataInformation) {
		try {
			int calculatedArea = FactoryShape.createShape(nameOfShape, dataInformation).calculatArea();
			writer.write(calculatedArea);

		} catch (ShapeCreationException e) {
			e.printStackTrace();
		}
	}
}
