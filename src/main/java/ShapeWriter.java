import java.io.StringWriter;

import com.app.test.jnut.utils.Writer;

public class ShapeWriter implements Writer {

	private StringWriter StringWriter;

	public ShapeWriter(java.io.StringWriter stringWriter) {
		StringWriter = stringWriter;
	}

	@Override
	public void write(final int area) {
		StringWriter.write(area + "\n");

	}

}
