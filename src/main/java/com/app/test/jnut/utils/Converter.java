package com.app.test.jnut.utils;

import java.util.ArrayList;
import java.util.List;

public class Converter {
	public static List<Integer> converterToIntArrayList(List<String> values) {
		List<Integer> intList = new ArrayList<>();
		for (String s : values) {
			intList.add(Integer.valueOf(s));
		}
		return intList;
	}
}
