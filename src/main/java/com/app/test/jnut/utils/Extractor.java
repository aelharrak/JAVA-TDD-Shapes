package com.app.test.jnut.utils;

import java.util.Arrays;
import java.util.List;

public class Extractor {

	private final static String DELIMETER = ",";

	public static String[] extract(String stringValue) {
		return stringValue.split(DELIMETER);
	}

	public static List<String> extractIntoList(String stringValue) {
		String[] values = extract(stringValue);
		return Arrays.asList(values);
	}

	public static List<Integer> converterIntoIntegerArrayList(String value) {
		List<String> intList = extractIntoList(value);
		return Converter.converterToIntArrayList(intList);
	}

}
