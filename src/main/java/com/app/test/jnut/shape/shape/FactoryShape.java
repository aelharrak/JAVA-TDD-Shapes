package com.app.test.jnut.shape.shape;

import java.util.List;

import com.app.test.jnut.shape.exception.ShapeCreationException;
import com.app.test.jnut.utils.Extractor;

public class FactoryShape {

	public static Shape createShape(String shapeString, String dataInformation) throws ShapeCreationException {

		List<Integer> dimentions = Extractor.converterIntoIntegerArrayList(dataInformation);

		switch (shapeString) {

		case "SQUARE":
			int radius = dimentions.get(0);
			return new Square(radius);

		case "RECTANGLE":

			int width = dimentions.get(0);
			int height = dimentions.get(1);
			return new Rectangle(width, height);

		case "TRIANGLE":
			int base = dimentions.get(0);
			if (dimentions.size() == 1) {
				return new Triangle(base);
			} else {
				int altitude = dimentions.get(1);
				return new Triangle(base, altitude);
			}
		default:
			throw new ShapeCreationException("Impossible de cr�er un " + shapeString);
		}
	}
}
