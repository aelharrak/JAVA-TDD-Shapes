package com.app.test.jnut.shape.exception;

@SuppressWarnings("serial")
public class ShapeCreationException extends Exception {

	public ShapeCreationException() {
	}

	public ShapeCreationException(String message) {
		super(message);
	}

}
