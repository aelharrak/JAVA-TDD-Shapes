package com.app.test.jnut.shape.shape;

public class Rectangle extends Shape {

	private final int width;
	private final int height;

	public Rectangle(final int width, final int height) {
		this.width = width;
		this.height = height;
	}

	public int calculatArea() {
		return width * height;

	}

}
