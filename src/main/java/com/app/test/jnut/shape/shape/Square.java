package com.app.test.jnut.shape.shape;

public class Square extends Shape {

	private final int radius;

	public Square(final int ray) {
		this.radius = ray;
	}

	@Override
	public int calculatArea() {
		return radius * radius;
	}

}
