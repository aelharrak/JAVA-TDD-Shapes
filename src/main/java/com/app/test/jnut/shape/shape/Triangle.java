package com.app.test.jnut.shape.shape;

public class Triangle extends Shape {

	private final int base;
	private final int altitude;

	public Triangle(final int width, final int height) {
		this.base = width;
		this.altitude = height;
	}

	public Triangle(final int base) {
		this.base = base;
		this.altitude = base;
	}

	public int calculatArea() {
		return (base * altitude) / 2;

	}

}
